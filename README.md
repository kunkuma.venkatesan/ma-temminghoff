# MA Temminghoff 

[![pipeline status](https://igm-git.igm.rwth-aachen.de/abschlussarbeiten/schmitz/ma-temminghoff/badges/main/pipeline.svg)](https://igm-git.igm.rwth-aachen.de/abschlussarbeiten/schmitz/ma-temminghoff/-/commits/main) 

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fkunkuma.venkatesan%2Fma-temminghoff/HEAD?labpath=RoboticSystemsLecture.ipynb)

## About

This repository is used to store all software requirements with the help of Dockerfile and enable end users to have access to software demonstration of the thesis with minimal setup time.

## Prerequisite
- Access to IGMR VPN
- Access to IGM-git docker registry

## Docker Setup
Please replace the $USERNAME, $PASSWORD with GitLab username and password, For other secure ways to log in with docker, check out this [link](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html).



```sh
# clone the repository
git clone https://igm-git.igm.rwth-aachen.de/abschlussarbeiten/schmitz/ma-temminghoff

# setup a local variable for igm-git container registry url
export IGM_DOCKER_REGISTRY_URL=igm-git.igm.rwth-aachen.de:4999

# setup a local variable for image url
export IMAGE_NAME="$IGM_DOCKER_REGISTRY_URL/abschlussarbeiten/schmitz/ma-temminghoff:latest"

# login to docker, make sure docker daemon is running
docker login -u $USERNAME -p $PASSWORD $IGM_DOCKER_REGISTRY_URL

# pull the docker image
docker pull $IMAGE_NAME

# run the docker image and click the link in the shell to open jupyterlab
docker run -rm -v PATH_TO_CLONED_REPO/thesis:/home/jovyan/thesis -p 8888:8888 -p 52000:52000 $IMAGE_NAME
```
